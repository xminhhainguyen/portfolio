const path = require('path')
const webpack = require('webpack')
const CompressionPlugin = require('compression-webpack-plugin')

module.exports = env => {
  const config = {
    entry: {
      browser: ['regenerator-runtime/runtime', path.join(__dirname, 'src/client.js')]
    },
    output: {
      path: path.join(__dirname, 'build/public/static'),
      publicPath: '/',
      filename: 'bundle.js'
    },
    devServer: {
      publicPath: '/',
      contentBase: './public',
      historyApiFallback: true,
      hot: true
    },
    target: 'web',
    mode: 'production',
    resolve: {
      alias: {
        components: path.resolve(__dirname, 'src/components'),
        constant: path.resolve(__dirname, 'src/constant'),
        scenes: path.resolve(__dirname, 'src/scenes')
      },
      extensions: ['*', '.js', '.jsx']
    },
    plugins: [],
    devtool: false,
    module: {
      rules: [
        {
          test: /\.(jsx|js)$/i,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: [
                  ['@babel/preset-env', {
                    targets: { browsers: ['last 2 versions'] }
                  }],
                  '@babel/preset-react'
                ]
              }
            }
          ]
        },
        {
          test: /\.css$/,
          use: ['css-loader']
        },
        {
          test: /\.(eot|woff|jpeg|png | svg|ttf|woff2|gif|appcache)(\?|$)/,
          exclude: /^node_modules$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]'
              }
            }
          ]
        },
        {
          test: /\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192
              }
            }
          ]
        },
        {
          test: /\.json$/,
          loader: 'json-loader'
        }
      ]
    }
  }

  if (env && env.NODE_ENV) {
    config.plugins = [
      ...config.plugins,
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(env.NODE_ENV)
        }
      })
    ]

    if (env.NODE_ENV === 'test' || env.NODE_ENV === 'production') {
      config.plugins = [
        ...config.plugins,
        new CompressionPlugin({
          deleteOriginalAssets: true,
          exclude: /lambda\.js/
        })
      ]
    }
  }

  return config
}
