const path = require('path')
const webpack = require('webpack')
const CompressionPlugin = require('compression-webpack-plugin')

module.exports = {
  entry: ['regenerator-runtime/runtime', path.join(__dirname, 'src/index.js')],
  target: 'node',
  mode: 'production',
  devtool: false,
  resolve: {
    mainFields: ['main', 'module'],
    alias: {
      components: path.resolve(__dirname, 'src/components'),
      constant: path.resolve(__dirname, 'src/constant'),
      scenes: path.resolve(__dirname, 'src/scenes')
    },
    extensions: ['*', '.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: ['css-loader']
      },
      {
        test: /\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$/,
        use: 'url-loader'
      }
    ]
  },
  output: {
    libraryTarget: 'commonjs2',
    path: path.join(__dirname, 'build'),
    filename: 'lambda.js'
  },
  plugins: [
    new CompressionPlugin({
      deleteOriginalAssets: true,
      exclude: /lambda\.js/
    }),
    new webpack.DefinePlugin({ 'global.GENTLY': false })
  ]
}
