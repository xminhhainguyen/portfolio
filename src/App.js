import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Layout, Tech, Home, Project } from 'scenes'
import { GlobalStyle } from './styled'
import { AnimatePresence } from 'framer-motion'

export default props => {
  return (
    <Layout>
      <AnimatePresence>
        <Switch>
          <Route exact path='/tech' component={Tech} />
          <Route exact path='/projects/:slug' component={Project} />
          <Route path='/' component={Home} />
        </Switch>
      </AnimatePresence>
      <GlobalStyle />
    </Layout>
  )
}
