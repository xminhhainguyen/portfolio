import styled from 'styled-components'
import { topPadding, breakpoints } from 'constant'

const { md } = breakpoints

export const Wrapper = styled.div`
  padding: ${topPadding} 0px;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  width: 800px;
  max-width: 100%;

  > * {
    margin-bottom: 12px;
  }

  > ul {
    > li {
      margin-bottom: 6px;
      line-height: 24px;
    }
  }

  > h2 {
    margin-top: 12px;
  }

  p {
    line-height: 24px;
  }

  ${md} {
    padding: 0px 32px;
  }
`

export const TechStack = styled.div`
  display: flex;
  flex-direction: column;

  ${md} {
    margin: 12px;
  }
`

export const TechStackWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  margin: 12px 0px;
  flex-wrap: wrap;
`
