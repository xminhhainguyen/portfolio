import React from 'react'
import { Wrapper, TechStack, TechStackWrapper, Content } from './styled'
import { TransitionWrapper, Interested } from 'components'
import { features, allTech, meaning } from './constant'
import { Link } from 'react-router-dom'
import { Helmet } from 'react-helmet'

const renderListItem = (item, idx) => {
  return <li key={idx}>{item}</li>
}

const renderTechStack = (stack, idx) => {
  const { title, tech } = stack

  return (
    <TechStack key={idx}>
      <h3>{title}</h3>
      <div>{tech.map(renderListItem)}</div>
    </TechStack>
  )
}

export default props => {
  return (
    <TransitionWrapper>
      <Wrapper>
        <Helmet>
          <title>Minh Nguyen - Tech</title>
        </Helmet>
        <Content>
          <h1>Tech Stack</h1>
          <p>When it comes to technology, I use whatever's needed to get the job done, but my preferred stack to work with is the one I've put together to build Serverless Universal Progressive Web Apps.</p>
          <h3>The apps built with my stack:</h3>
          <ul>
            {features.map(renderListItem)}
          </ul>
          <h3>This means:</h3>
          <ul>
            {meaning.map(renderListItem)}
          </ul>
          <TechStackWrapper>
            {allTech.map(renderTechStack)}
          </TechStackWrapper>
          <h2>Future Changes</h2>
          <p>I've recently picked up Hasura, an extremely powerful open-source engine that builds realtime GraphQL APIs instantly. With the shift to Hasura, I'll be integrating it into my back end stack as my main API and using serverless REST to build webhooks for Hasura Actions to handle custom business logic.</p>
          <p>With the shift to Hasura and GraphQL, I'll be utilizing Apollo Client in my front end stack now. Since Apollo Client can be used for global state, I'll be dropping Storeon and relying on Apollo Client instead.</p>
          <h2>Want to see what my code looks like?</h2>
          <p>Check out the <a rel='noopener noreferrer' href='https://gitlab.com/xminhhainguyen/portfolio' target='_blank'>codebase for this portfolio</a> or read up on my latest project <Link to='/projects/red-curtain-addict'>Red Curtain Addict</Link>.</p>
          <Interested />
        </Content>
      </Wrapper>
    </TransitionWrapper>
  )
}
