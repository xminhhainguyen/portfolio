export const features = [
  'Have the SEO and fast initial load of SSR (Server Side Rendered) App.',
  'Act as a SPA (Single Page Application) after the initial load for fast routes and a more dynamic user experience.',
  'Are deployed serverless for easy scaling and low hosting costs.',
  'Only take about 3 minutes from the start of build to being live. (Not counting any tests you want to run in your CI pipeline)',
  'Can be quickly wrapped in Ionic\'s Capacitor to gain access to native features and be built as an iOS or Android app using the same codebase.'
]

export const meaning = [
  'You get the benefits of both an SSR App and SPA, so you don\'t have to make a trade-off like most companies do and have to either patch up the SEO gaps of a SPA with snapshot tools or sacrifice UX with an SSR App.',
  'You\'ll only be paying for what you use vs. renting a server since the app is deployed serverless.',
  'You\'ll be saving a lot on development costs since you\'ll have a single codebase for Web, iOS, Android, PWA, and Desktop Native. In both terms of time saved not having to build another platform and additional development teams for each platform that you won\'t have to hire.',
  'The time saved means features will ship faster, and your app can grow more quickly.'
]

const frontendTech = {
  title: 'Frontend Tech',
  tech: [
    'React',
    'Storeon',
    'Styled Components'
  ]
}

const backendTech = {
  title: 'Backend Tech',
  tech: [
    'Node.js',
    'Express',
    'PostgreSQL'
  ]
}

const devopsTech = {
  title: 'DevOps Tech',
  tech: [
    'GitLab',
    'AWS',
    'Serverless'
  ]
}

export const allTech = [frontendTech, backendTech, devopsTech]
