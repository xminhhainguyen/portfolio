import React from 'react'
import { Wrapper, Left, Content, SocialLink, Right, Project } from './styled'
import { TransitionWrapper, Icon } from 'components'
import { socialLinks } from './constant'
import { projects } from 'constant'
import { Helmet } from 'react-helmet'

const { projectsArr } = projects

const renderLink = (item, idx) => {
  const { svg, link } = item

  return (
    <SocialLink key={idx} href={link} rel='noopener noreferrer' target='_blank'>
      <Icon name={svg} />
    </SocialLink>
  )
}

const renderProject = (item, idx) => {
  const { name, slug, tagline, logo } = item

  return (
    <Project key={idx} to={`/projects/${slug}`} logo={logo} delay={idx}>
      <div />
      <div>
        <h3>{name}</h3>
        <h4>{tagline}</h4>
      </div>
    </Project>
  )
}

export default props => {
  return (
    <TransitionWrapper>
      <Wrapper>
        <Helmet>
          <title>Minh Nguyen</title>
        </Helmet>
        <Content>
          <Left>
            <h1>Min<span>h Ngu</span>yen</h1>
            <h2>Software Architect</h2>
            <div>
              {socialLinks.map(renderLink)}
            </div>
          </Left>
          <Right>
            <h2>Recent Projects</h2>
            <div>
              {projectsArr.map(renderProject)}
            </div>
          </Right>
        </Content>
      </Wrapper>
    </TransitionWrapper>
  )
}
