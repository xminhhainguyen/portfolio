export const socialLinks = [
  { svg: 'linkedin', link: 'https://www.linkedin.com/in/minyens/' },
  { svg: 'code', link: 'https://gitlab.com/xminhhainguyen/portfolio' },
  { svg: 'email', link: 'mailto:minh@kintsugi.network' }
]
