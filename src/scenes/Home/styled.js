import styled, { keyframes } from 'styled-components'
import { colors, breakpoints, topPadding } from 'constant'
import { Link } from 'react-router-dom'

const { black } = colors
const { md } = breakpoints

const rotateIn = keyframes`
  0% {
    transform: translate(40px, 40px) rotate(-25deg);
    opacity: 0;
  }
  100% {
    transform: translate(0px, 0px) rotate(0deg);
    opacity: 1;
  }
`

const fade = keyframes`
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0.2;
  }
`

export const Wrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${topPadding} 0px;
`

export const Left = styled.div`
  display: flex;
  flex-direction: column;
  border-left: 4px solid ${black};
  padding: 24px 32px;
  width: 460px;
  height: 200px;

  > h1 {
    font-size: 52px;
    margin-bottom: 4px;

    > span {
      animation: ${fade} 1s forwards;
      animation-delay: 1s;
    }
  }

  > h2 {
    margin-bottom: 12px;
  }

  > div {
    display: flex;
    align-self: flex-end;

    > a {
      &:not(:last-child) {
        margin-right: 12px;
      }
    }
  }

  ${md} {
    height: auto;
    margin-bottom: 48px;
    margin-top: 16px;
  }
`

export const Right = styled.div`
  flex: 1;
  border-left: 4px solid ${black};
  padding: 24px 32px;
  
  > h2 {
    margin-bottom: 12px;
  }

  > div {
    margin-left: 12px;

    > a {
      &:nth-child(2) {
        margin-left: 24px;
      }

      &:nth-child(3) {
        margin-left: 24px;
      }

      &:not(:last-child) {
        margin-bottom: 18px;
      }
    }
  }

  ${md} {
    border-left: none;
    border-right: 4px solid ${black};
  }
`

export const Content = styled.div`
  width: 1000px;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  max-width: 100%;

  ${md} {
    padding: 0px 24px;
  }
`

export const SocialLink = styled.a`
  color: ${black};

  svg {
    height: 34px;
    width: 34px;
  }
`

export const Project = styled(Link)`
  color: ${black};
  text-decoration: none;
  display: flex;
  align-items: center;
  animation: ${rotateIn} 0.5s both;
  animation-delay: ${({ delay }) => 200 * delay}ms;

  > div {
    &:first-child {
      height: 65px;
      width: 75px;
      margin-right: 12px;
      background-image: url(${({ logo }) => logo});
      background-size: contain;
      background-position: center;
      background-repeat: no-repeat;
    }
  }
  
  ${md} {
    > div {
      > h3 {
        font-size: 16px;
      }

      > h4 {
        font-size: 14px;
        padding-left: 12px;
      }
    }
  }
`
