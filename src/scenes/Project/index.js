import React from 'react'
import { Wrapper, Card, CardContent, Banner, Dash } from './styled'
import { useParams, Link } from 'react-router-dom'
import { TransitionWrapper, Gallery, Interested } from 'components'
import { Helmet } from 'react-helmet'
import { projects } from 'constant'

const { projectsSlugMap } = projects

const renderListItem = (item, idx) => {
  return <li key={idx}>{item}</li>
}

const renderBuiltWith = isTrue => {
  if (!isTrue) return null

  return (
    <h3>
      Built with my <Link to='/tech'>Serverless Universal Progressive Web App Stack</Link>.
    </h3>
  )
}

const renderUrl = link => {
  if (!link) return null

  return (
    <h3>
      <a href={link} rel='noopener noreferrer' target='_blank'>Visit the site!</a>
    </h3>
  )
}

export default props => {
  const { slug } = useParams()
  const selectedProject = projectsSlugMap[slug]
  const { name, url, builtWithStack, background, tagline, images, duties, color } = selectedProject

  return (
    <TransitionWrapper>
      <Helmet>
        <title>Minh Nguyen - {name}</title>
      </Helmet>
      <Wrapper background={background}>
        <div>
          <h1>{name}</h1>
          <Dash color={color} />
          <h2>{tagline}</h2>
          <Card>
            <Banner background={background}>
              <Gallery images={images} />
              {renderUrl(url)}
            </Banner>
            <CardContent background={background}>
              {renderBuiltWith(builtWithStack)}
              <h2>What I did:</h2>
              <ul>
                {duties.map(renderListItem)}
              </ul>
              <Interested />
            </CardContent>
          </Card>
        </div>
      </Wrapper>
    </TransitionWrapper>
  )
}
