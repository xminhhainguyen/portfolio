import styled from 'styled-components'
import { topPadding, colors, breakpoints } from 'constant'

const { black } = colors
const { md } = breakpoints

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  min-height: 100vh;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  padding: ${topPadding} 0px;
  overflow: auto;
  background-image:  linear-gradient(rgba(255, 255, 255, 0.8), rgba(255, 255, 255, 0.8)), url(${({ background }) => background});

  > div {
    display: flex;
    flex-direction: column;
    align-items: center;

    > h2 {
      margin-bottom: 16px;
      font-size: 20px;
    }

    ${md} {
      > h1 {
        font-size: 24px;
      }

      > h2 {
        font-size: 16px;
      }
    }
  }
`

export const Dash = styled.div`
  width: 200px;
  height: 3px;
  margin: 6px 0px;
  background: ${({ color }) => color};
`

export const Banner = styled.div`
  background-image:  linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url(${({ background }) => background});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 32px 0px;

  > h3 {
    margin-top: 24px;
    > a {
      color: white;
    }
  }

  ${md} {
    padding: 24px 18px;
  }
`

export const Card = styled.div`
  width: 1000px;
  background: rgba(255, 255, 255, 0.9);
  border-radius: 10px;
  box-shadow: 2px 2px 4px 4px rgba(0, 0, 0, 0.2);
  color: ${black};
  position: relative;
  overflow: hidden;
  max-width: 95vw;
`

export const CardContent = styled.div`
  height: 100%;
  width: 100%;
  padding: 32px;

  > h3 {
    margin-bottom: 12px;
  }

  > h2 {
    margin-bottom: 12px;
  }

  > ul {
    > li {
      margin-bottom: 6px;
      line-height: 24px;
    }
  }
`
