export { default as Layout } from './Layout'
export { default as Home } from './Home'
export { default as Project } from './Project'
export { default as Tech } from './Tech'
