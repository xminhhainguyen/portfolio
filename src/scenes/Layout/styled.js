import styled from 'styled-components'
import { colors } from 'constant'

const { black } = colors

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  min-height: 100vh;
  background-size: cover;
  background-position: center;
  color: ${black};
  overflow-x: hidden;
  position: relative;

  > div {
    &:nth-child(2) {
      height: 100%;
      width: 100%;
    }
  }

  a {
    color: ${black};
  }
`
