import React, { useState, useEffect } from 'react'
import { Wrapper } from './styled'
import { Link } from 'react-router-dom'

let prevScroll

export default props => {
  const [hideHeader, setHideHeader] = useState(false)
  const [headerBg, setHeaderBg] = useState(false)

  const checkScroll = e => {
    if (window.scrollY > 0) {
      if (prevScroll > window.scrollY) {
        setHideHeader(false)
      } else {
        setHideHeader(true)
      }
    } else {
      setHideHeader(false)
    }

    if (window.scrollY === 0) {
      setHeaderBg(false)
    } else {
      setHeaderBg(true)
    }

    prevScroll = window.scrollY
  }

  useEffect(() => {
    window.addEventListener('scroll', checkScroll)

    return () => {
      window.removeEventListener('scroll', checkScroll)
    }
  }, [])

  return (
    <Wrapper hide={hideHeader} headerBg={headerBg}>
      <Link to='/'>
        Minyen
      </Link>
      <div>
        <Link to='/tech'>Tech</Link>
      </div>
    </Wrapper>
  )
}
