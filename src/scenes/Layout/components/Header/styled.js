import styled from 'styled-components'
import { colors } from 'constant'

const { black } = colors

export const Wrapper = styled.header`
  width: 100vw;
  max-width: 100%;
  position: fixed;
  top: 0px;
  left: 0px;
  z-index: 1;
  color: ${black};
  padding: 24px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-bottom: 64px;
  align-items: center;
  transform: translateY(0px);
  transition: all 300ms ease;
  background: rgba(0, 0, 0, 0);

  a {
    color: ${black};
    text-decoration: none;
    font-weight: 600;
    font-size: 18px;
  }

  > div {
    &:last-child {
      display: flex;
      align-items: center;

      > a {
        &:not(:last-child) {
          margin-right: 12px;
        }
      }
    }
  }

  ${({ hide }) => hide && 'transform: translateY(-220px);'}
  ${({ headerBg }) => headerBg && `
    background: black;
    a {
      color: white;
    }
  `}
`
