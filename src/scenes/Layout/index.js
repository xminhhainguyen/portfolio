import React, { useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import { Wrapper } from './styled'
import { Header } from './components'

export default props => {
  const { pathname } = useLocation()

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [pathname])

  return (
    <Wrapper>
      <Header />
      {props.children}
    </Wrapper>
  )
}
