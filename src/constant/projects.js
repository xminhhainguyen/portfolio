const redCurtainAddict = {
  name: 'Red Curtain Addict',
  url: 'https://redcurtainaddict.com/',
  builtWithStack: true,
  slug: 'red-curtain-addict',
  background: 'https://minyen.imgix.net/rca.jpg?auto=compression&height=500&blur=50&sat=-20',
  tagline: 'Your source to experience the arts',
  color: '#C80228',
  logo: 'https://minyen.imgix.net/rca-b.png?auto=compression&height=100',
  duties: [
    'Single-handedly took care of the entire development cycle, including frontend, backend, devops, design, and project management.',
    'Took high-level ideas and concepts, boiled them down to their core, and polished them into features.',
    'Architected and built the database needed for these features in PostgreSQL and API endpoints in Node.js.',
    'Translated these features into designs, going directly from conceptualization to code, allowing the feedback loop to progress very quickly and let us iterate faster the typical development cycle.',
    'Built out an administration panel to manage data and also migrated existing articles and other data from their old WordPress site to their new database.',
    'Setup a deployment pipeline within GitLab to pull a custom docker image, build, and deploy the app upon merging, taking only 3 minutes from merging to being live.',
    'Followed PWA guidelines to allow for the app to be installed on mobile home screens.'
  ],
  images: [
    'https://minyen.imgix.net/rca-1.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/rca-2.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/rca-admin.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/rca-3.jpg?auto=compression&height=400'
  ]
}

const strainGenie = {
  name: 'StrainGenie',
  builtWithStack: true,
  slug: 'strain-genie',
  background: 'https://minyen.imgix.net/sg-kit.jpg?auto=compression&height=500&blur=50&sat=-20',
  tagline: 'Your Personal Cannabis DNA Test',
  color: '#244656',
  logo: 'https://minyen.imgix.net/sg-b.png?auto=compression&height=100',
  duties: [
    'Single-handedly took care of the entire development cycle, including frontend, backend, devops, design, and project management.',
    'Took high-level ideas and concepts, boiled them down to their core, and polished them into features.',
    'Architected and built the database needed for these features in PostgreSQL and API endpoints in Node.js.',
    'Translated these features into designs, going directly from conceptualization to code, allowing the feedback loop to progress very quickly and let us iterate faster the typical development cycle.',
    'Built out an administration panel to manage report data.',
    'Built a sync feature within the administration panel to allow for processed reports to be uploaded and synced the corresponding user if they exist or send out a signup link with a generated token for them to create and an account and sync their data if not.',
    'Setup a deployment pipeline within GitLab to pull a custom docker image, build, and deploy the app upon merging, taking only 3 minutes from merging to being live.',
    'Followed PWA guidelines to allow for the app to be installed on mobile home screens.'
  ],
  images: [
    'https://minyen.imgix.net/sg-1.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/sg-2.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/sg-3.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/sg-4.jpg?auto=compression&height=400'
  ]
}

const phci = {
  name: 'PhCI',
  builtWithStack: true,
  slug: 'phci',
  background: 'https://minyen.imgix.net/phcibg.png?auto=compression&height=500&blur=50&sat=-20',
  tagline: 'A consciousness tracking app',
  color: '#BDDEDE',
  logo: 'https://minyen.imgix.net/phci-c.png?auto=compression&height=100',
  duties: [
    'Took care of the entire engineering cycle, including frontend, backend, devops',
    'Architected and built the database needed for the app in PostgreSQL and API endpoints in Node.js.',
    'Built out pixel perfect UIs based on the given designs',
    'Helped brainstorm UI/UX improvements.',
    'Built data visualizations to represent user qualias.',
    'Setup a deployment pipeline within GitLab to pull a custom docker image, build, and deploy the app upon merging, taking only 3 minutes from merging to being live.',
    'Followed PWA guidelines to allow for the app to be installed on mobile home screens.',
    'Utilized Ionic\'s Capacitor to build the app as a native Android and iOS app.'
  ],
  images: [
    'https://minyen.imgix.net/phci-3.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/phci-1.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/phci2.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/phci4.jpg?auto=compression&height=400'
  ]
}

const woahStork = {
  name: 'WoahStork',
  slug: 'woahstork',
  background: 'https://minyen.imgix.net/ws-bg.jpg?auto=compression&height=500&blur=50&sat=-20',
  tagline: 'Your Online Cannabis Marketplace',
  color: '#639036',
  logo: 'https://minyen.imgix.net/ws-b.png?auto=compression&height=100',
  duties: [
    'Migrated the company’s infrastructure to a serverless architecture, reducing their monthly AWS bill from thousands a month to hundreds while also significantly speeding up deployment and build times.',
    'Managed a team of developers consisting of both local and overseas developers as the CTO, planning out sprints, making sure that goals are perfectly clear, tasks that are considered blockers are identified as so and prioritized, and everyone’s being utilized effectively based on their talents.',
    'Redesigned and built out the marketplace and admin panel applications utilizing the latest technology and best practices while taking into account aesthetics and UX, landing several brand accounts after impressing them with the best design they’ve seen.',
    'Single handedly built out several features at an extremely fast pace to satisfy clients, one of which being an external product locator map which is an enriched version of WeedMap’s core functionality and was built out, tested, and deployed to production in only a week.'
  ],
  images: [
    'https://minyen.imgix.net/ws-1.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/ws-2.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/ws-4.jpg?auto=compression&height=400',
    'https://minyen.imgix.net/ws-3.jpg?auto=compression&height=400'
  ]
}

const projectsArr = [redCurtainAddict, strainGenie, phci, woahStork]

const projectsSlugMap = {
  'red-curtain-addict': redCurtainAddict,
  'strain-genie': strainGenie,
  phci: phci,
  woahstork: woahStork
}

export default {
  projectsArr,
  projectsSlugMap
}
