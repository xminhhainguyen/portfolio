export { default as colors } from './colors'
export { default as breakpoints } from './breakpoints'
export { default as projects } from './projects'

export const topPadding = '64px'
