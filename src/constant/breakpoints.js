export default {
  sm: '@media (max-width: 600px)',
  md: '@media (max-width: 960px)',
  lg: '@media (max-width: 1280px)',
  xl: '@media (max-width: 1920px)'
}
