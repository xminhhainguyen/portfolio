import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    margin: 0px;
    -webkit-tap-highlight-color: transparent;
    font-family: 'Montserrat';
  }

  &:focus {
    outline: none;
  }
`
