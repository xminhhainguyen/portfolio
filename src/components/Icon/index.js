import React from 'react'
import ReactSVG from 'react-svg'

const Icon = props => {
  const { name, onClick } = props

  return (
    <ReactSVG
      onClick={onClick}
      src={`https://minyen.imgix.net/svgs/${name}.svg`}
    />
  )
}

export default Icon
