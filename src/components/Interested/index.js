import React from 'react'
import { Wrapper } from './styled'

export default props => {
  return (
    <Wrapper>
      <h2>Interested in working together?</h2>
      <p>Shoot me an email: <a href='mailto:minh@kintsugi.network'>minh@kintsugi.network</a></p>
    </Wrapper>
  )
}
