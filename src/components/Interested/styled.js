import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 12px;

  > h2 {
    margin-bottom: 12px;
  }
`
