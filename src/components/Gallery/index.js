import React, { useState } from 'react'
import { Wrapper, MainImage, Images, ImageItem } from './styled'

export default props => {
  const [currImg, setCurrImg] = useState(0)

  const { images } = props

  const renderImageItem = (item, idx) => {
    return (
      <ImageItem
        key={idx}
        url={item}
        onClick={() => setCurrImg(idx)}
        active={idx === currImg}
      />
    )
  }

  return (
    <Wrapper>
      <MainImage key={currImg} url={images[currImg]} src={images[currImg]} />
      <Images>
        {images.map(renderImageItem)}
      </Images>
    </Wrapper>
  )
}
