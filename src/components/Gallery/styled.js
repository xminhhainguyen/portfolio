import styled, { keyframes } from 'styled-components'

const slideIn = keyframes`
  0% {
    transform: translateY(12px);
    opacity: 0;
  }
  100% {
    transform: translateY(0px);
    opacity: 1;
  }
`

export const Wrapper = styled.div`
  width: 700px;
  max-width: 100%;
`

export const MainImage = styled.img`
  width: 100%;
  margin-bottom: 12px;
  animation: ${slideIn} 0.5s both;
  border-radius: 4px;
`

export const Images = styled.div`
  display: flex;
`

export const ImageItem = styled.button`
  background-image: url(${({ url }) => url});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  flex: 1;
  height: 100px;
  margin: 0px 6px;
  border: none;
  border-radius: 4px;
  transform: scale(1);
  transition: transform 300ms ease;

  ${({ active }) => active && 'transform: scale(1.05)'}
`
