import * as React from 'react'
import * as path from 'path'
import * as fs from 'fs'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import { ServerStyleSheet } from 'styled-components'
import App from './App'

const map = {
  '.ico': 'image/x-icon',
  '.html': 'text/html',
  '.js': 'application/javascript',
  '.json': 'application/json',
  '.css': 'text/css',
  '.png': 'image/png',
  '.jpg': 'image/jpeg',
  '.wav': 'audio/wav',
  '.mp3': 'audio/mpeg',
  '.svg': 'image/svg+xml',
  '.pdf': 'application/pdf',
  '.doc': 'application/msword'
}

export const renderApp = async (event, context) => {
  const url = event.path
  const ext = path.extname(url)

  const staticRouterContext = {}
  const sheet = new ServerStyleSheet()
  const markup = await renderToString(
    sheet.collectStyles(
      <StaticRouter context={staticRouterContext} location={url}>
        <App />
      </StaticRouter>
    )
  )

  const styleTags = sheet.getStyleTags()

  const html = `
    <!doctype html>
      <html lang="">
        <head>
          <meta http-equiv="X-UA-Compatible" content="IE=edge" />
          <meta charset="utf-8" />
          <title>Minh Nguyen</title>
          ${styleTags}
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600" rel="stylesheet" />
          ${process.env.NODE_ENV === 'production'
            ? '<script src="/static/bundle.js" defer></script>'
            : '<script src="/static/bundle.js" defer crossorigin></script>'
          }
        </head>
        <body>
          <div id="root">${markup}</div>
        </body>
      </html>
    `

  const basePath = './build/public'
  const resource = (url === '/') ? (basePath + '/index.html') : (basePath + url)

  if (url === '/static/bundle.js') {
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'application/javascript',
        'Content-Encoding': 'gzip'
      },
      isBase64Encoded: true,
      body: fs.readFileSync(`${resource}.gz`).toString('base64')
    }
  }

  if (fs.existsSync(resource)) {
    return {
      statusCode: 200,
      headers: {
        'Content-Type': map[ext]
      },
      body: fs.readFileSync(resource, { encoding: 'utf8' })
    }
  }

  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/html'
    },
    body: html
  }
}
